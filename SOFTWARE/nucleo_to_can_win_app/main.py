import tkinter as tk
import ttkbootstrap as ttk
from ttkbootstrap.constants import *
from ttkbootstrap.scrolled import ScrolledText
import serial
import serial.tools.list_ports
import threading
import re


def get_serial_ports():
    ports = serial.tools.list_ports.comports()
    return [(port.device, port.description) for port in ports]


class CANApp:
    def __init__(self, root_app):
        self.root = root_app
        self.root.title("NUCLEO_TO_CAN V1.0")
        self.root.resizable(False, False)  # Disable window resizing
        self.it = 0  # Number of frames

        self.data_store = {}  # Store data for each ID

        # Frame for UART Selection and received messages
        uart_frame = ttk.LabelFrame(root_app, text="UART Selection")
        uart_frame.grid(row=0, column=0, padx=10, pady=(10, 10), sticky="ew")

        self.uart_label = ttk.Label(uart_frame, text="UART Port:")
        self.uart_label.grid(row=0, column=0, padx=5, pady=5, sticky="e")

        self.uart_combobox = ttk.Combobox(uart_frame, width=50)
        self.uart_combobox.grid(row=0, column=1, padx=5, pady=5, sticky="w")
        self.update_serial_ports()

        self.baudrate_label = ttk.Label(uart_frame, text="Baud Rate:")
        self.baudrate_label.grid(row=1, column=0, padx=5, pady=15, sticky="e")

        baudrate_val = list(map(str, [4800, 9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600]))
        self.baudrate_combobox = ttk.Combobox(uart_frame, values=baudrate_val, width=50)
        self.baudrate_combobox.grid(row=1, column=1, padx=5, pady=15, sticky="w")
        self.baudrate_combobox.current(5)  # Set default baud rate

        # Button to open UART port
        self.open_uart_button = ttk.Button(uart_frame, text="Open UART", command=self.open_uart)
        self.open_uart_button.grid(row=2, column=1, padx=5, pady=10, sticky="w")

        # Button to close UART port
        self.close_uart_button = ttk.Button(uart_frame, text="Close UART", command=self.close_uart)
        self.close_uart_button.grid(row=2, column=1, padx=5, pady=10, sticky="n")
        self.close_uart_button.config(state=tk.DISABLED)

        # Button to update serial ports list
        self.update_ports_button = ttk.Button(uart_frame, text="Update Ports", command=self.update_serial_ports)
        self.update_ports_button.grid(row=2, column=1, padx=5, pady=10, sticky="e")

        # Text area for received messages
        self.receive_text = ScrolledText(uart_frame, height=10, width=65, bootstyle="rounded")
        self.receive_text.grid(row=4, column=0, columnspan=3, padx=5, pady=10, sticky="se")

        # Error message label at the bottom right
        self.status_label = ttk.Label(root_app, text="", foreground="white", relief="sunken", anchor="w",
                                      font=('Arial', 15))
        self.status_label.grid(row=2, column=0, columnspan=2, sticky="ew")

        # Frame for CAN Frame Settings
        can_frame = ttk.LabelFrame(root_app, text="CAN Frame Settings")
        can_frame.grid(row=1, column=0, padx=10, pady=(5, 10), sticky="ew")

        self.id_label = ttk.Label(can_frame, text="Identifier:")
        self.id_label.grid(row=0, column=0, padx=5, pady=5, sticky="e")

        self.id_entry = ttk.Entry(can_frame, width=50)
        self.id_entry.grid(row=0, column=1, padx=5, pady=5, sticky="w")

        self.length_label = ttk.Label(can_frame, text="Data Length:")
        self.length_label.grid(row=1, column=0, padx=5, pady=5, sticky="e")

        # Replace Entry with Combobox for Data Length
        length_options = [
            "FDCAN_DLC_BYTES_0", "FDCAN_DLC_BYTES_1", "FDCAN_DLC_BYTES_2",
            "FDCAN_DLC_BYTES_3", "FDCAN_DLC_BYTES_4", "FDCAN_DLC_BYTES_5",
            "FDCAN_DLC_BYTES_6", "FDCAN_DLC_BYTES_7", "FDCAN_DLC_BYTES_8",
            "FDCAN_DLC_BYTES_12", "FDCAN_DLC_BYTES_16", "FDCAN_DLC_BYTES_20",
            "DDCAN_DLC_BYTES_24", "FDCAN_DLC_BYTES_32", "FDCAN_DLC_BYTES_48",
            "FDCAN_DLC_BYTES_64"
        ]
        self.length_combobox = ttk.Combobox(can_frame, values=length_options, width=30)
        self.length_combobox.grid(row=1, column=1, padx=5, pady=5, sticky="w")
        self.length_combobox.current(8)  # Set default bit length

        # Dictionary to map Combobox values to their corresponding numeric values
        self.length_value_map = {
            "FDCAN_DLC_BYTES_0": 0, "FDCAN_DLC_BYTES_1": 1, "FDCAN_DLC_BYTES_2": 2,
            "FDCAN_DLC_BYTES_3": 3, "FDCAN_DLC_BYTES_4": 4, "FDCAN_DLC_BYTES_5": 5,
            "FDCAN_DLC_BYTES_6": 6, "FDCAN_DLC_BYTES_7": 7, "FDCAN_DLC_BYTES_8": 8,
            "FDCAN_DLC_BYTES_12": 9, "FDCAN_DLC_BYTES_16": 10, "FDCAN_DLC_BYTES_20": 11,
            "DDCAN_DLC_BYTES_24": 12, "FDCAN_DLC_BYTES_32": 13, "FDCAN_DLC_BYTES_48": 14,
            "FDCAN_DLC_BYTES_64": 15
        }

        self.length_value_map_bytes = {
            "FDCAN_DLC_BYTES_0": 0, "FDCAN_DLC_BYTES_1": 1, "FDCAN_DLC_BYTES_2": 2,
            "FDCAN_DLC_BYTES_3": 3, "FDCAN_DLC_BYTES_4": 4, "FDCAN_DLC_BYTES_5": 5,
            "FDCAN_DLC_BYTES_6": 6, "FDCAN_DLC_BYTES_7": 7, "FDCAN_DLC_BYTES_8": 8,
            "FDCAN_DLC_BYTES_12": 12, "FDCAN_DLC_BYTES_16": 16, "FDCAN_DLC_BYTES_20": 20,
            "DDCAN_DLC_BYTES_24": 24, "FDCAN_DLC_BYTES_32": 32, "FDCAN_DLC_BYTES_48": 48,
            "FDCAN_DLC_BYTES_64": 64
        }

        self.word_length_label = ttk.Label(can_frame, text="Word Length:")
        self.word_length_label.grid(row=2, column=0, padx=5, pady=5, sticky="e")

        # Combobox for Word Length
        word_length_options = ["8-bit", "12-bit", "16-bit", "32-bit"]
        self.word_length_combobox = ttk.Combobox(can_frame, values=word_length_options, width=30)
        self.word_length_combobox.grid(row=2, column=1, padx=5, pady=5, sticky="w")
        self.word_length_combobox.current(0)  # Set default word length to 8-bit

        # Dictionary to map Combobox values to their corresponding numeric values
        self.word_length_value_map = {
            "8-bit": 255, "12-bit": 4095, "16-bit": 65535, "32-bit": 4294967295
        }

        self.data_label = ttk.Label(can_frame, text="Data:")
        self.data_label.grid(row=3, column=0, padx=5, pady=5, sticky="e")

        self.data_entry = ttk.Entry(can_frame, width=50)
        self.data_entry.grid(row=3, column=1, padx=5, pady=5, sticky="w")

        self.send_button = ttk.Button(can_frame, text="SEND", command=self.send_data, bootstyle="success")
        self.send_button.grid(row=4, column=0, columnspan=4, padx=10, pady=10, sticky='ew')

        # Frame for Received Data
        receive_frame = ttk.LabelFrame(root_app, text="Received Data")
        receive_frame.grid(row=0, column=1, rowspan=2, padx=5, pady=10, sticky="nsew")

        # Labels for received data
        ttk.Label(receive_frame, text="Received IDs:").grid(row=0, column=0, padx=5, pady=5, sticky="w")
        ttk.Label(receive_frame, text="Data Display:").grid(row=0, column=1, padx=5, pady=5, sticky="w")

        # Configure grid to allow dynamic resizing
        receive_frame.grid_rowconfigure(1, weight=1)
        receive_frame.grid_columnconfigure(0, weight=1)
        receive_frame.grid_columnconfigure(1, weight=2)

        # Received IDs listbox with scrollbars
        self.id_listbox = tk.Listbox(receive_frame, width=30)
        self.id_listbox.grid(row=1, column=0, padx=5, pady=5, sticky="nsew")
        id_scrollbar = ttk.Scrollbar(receive_frame, orient="vertical", command=self.id_listbox.yview)
        id_scrollbar.grid(row=1, column=1, sticky="nse")
        self.id_listbox.config(yscrollcommand=id_scrollbar.set)
        self.id_listbox.bind('<<ListboxSelect>>', self.on_id_select)

        # Data display area with scrollbars
        self.data_display = tk.Listbox(receive_frame, width=90)
        self.data_display.grid(row=1, column=1, padx=5, pady=5, sticky="nsew")
        data_scrollbar = ttk.Scrollbar(receive_frame, orient="vertical", command=self.data_display.yview)
        data_scrollbar.grid(row=1, column=2, sticky="nse")
        self.data_display.config(yscrollcommand=data_scrollbar.set)

        # Serial port
        self.serial_port = None
        self.stop_event = threading.Event()

        # Start reading thread
        self.read_thread = threading.Thread(target=self.read_data)
        self.read_thread.start()

    def update_serial_ports(self):
        ports = get_serial_ports()
        self.uart_combobox['values'] = [f"{port[0]} - {port[1]}" for port in ports]
        if ports:
            self.uart_combobox.current(0)

    def open_uart(self):
        try:
            if self.serial_port and self.serial_port.is_open:
                self.update_status(f"Error: UART port is already open.", is_error=True)
                self.close_uart()

            selected_port = self.uart_combobox.get().split(' - ')[0]
            selected_baudrate = int(self.baudrate_combobox.get())
            self.serial_port = serial.Serial(selected_port, selected_baudrate, timeout=1)
            self.update_status(f"Opened UART port {selected_port} successfully.")
            self.open_uart_button.config(state=tk.DISABLED)
            self.close_uart_button.config(state=tk.NORMAL)
        except Exception as e:
            self.update_status(f"Error: {str(e)}", is_error=True)

    def close_uart(self):
        try:
            if self.serial_port and self.serial_port.is_open:
                self.serial_port.close()
                self.update_status("Closed UART port successfully.")
                self.open_uart_button.config(state=tk.NORMAL)
                self.close_uart_button.config(state=tk.DISABLED)
            else:
                self.update_status(f"Error: UART port is not open.", is_error=True)
        except Exception as e:
            self.update_status(f"Error: {str(e)}", is_error=True)

    def send_data(self):
        try:
            if not self.serial_port or not self.serial_port.is_open:
                self.update_status(f"Error: UART port is not open.", is_error=True)
                return

            can_id = self.id_entry.get()
            data_length_label = self.length_combobox.get()  # Use Combobox value
            data_length = self.length_value_map.get(data_length_label, 0)  # Map to numeric value
            data_length_bytes = self.length_value_map_bytes.get(data_length_label, 0)
            word_length_label = self.word_length_combobox.get()  # Use Combobox value
            word_length_max_value = self.word_length_value_map.get(word_length_label, 255)  # Map to max value
            data = self.data_entry.get()

            # Strip leading and trailing spaces and convert multiple spaces into ;
            data = data.strip().replace(' ', ';')
            # Replace multiple ; with a single ;
            data = re.sub(r';+', ';', data)

            # Split the data by ;
            data_elements = data.split(';')

            # Convert elements starting with 0x or 0o
            converted_data_elements = []
            for element in data_elements:
                if element.startswith('0x'):
                    converted_data_elements.append(int(element, 16))
                elif element.startswith('0o'):
                    converted_data_elements.append(int(element, 8))
                else:
                    converted_data_elements.append(int(element))

            # Validate data length
            if len(converted_data_elements) != data_length_bytes:
                self.update_status(
                    f"Error: Number of elements ({len(converted_data_elements)})"
                    f" does not match selected Data Length ({data_length_bytes} : {data_length_label}).", is_error=True)
                return

            # Validate each element based on the selected word length
            if any(element > word_length_max_value for element in converted_data_elements):
                self.update_status(f"Error: Data contains values exceeding {word_length_max_value} "
                                   f"for {word_length_label}.", is_error=True)
                return

            # Construct the frame to send
            frame = f"{can_id};{data_length};{';'.join(map(str, converted_data_elements))};"
            self.receive_text.insert(END, f"{self.it}. TX >> {frame}\n")
            self.receive_text.see(END)
            frame += '\r\n'
            self.serial_port.write(frame.encode('utf-8'))
            self.update_status(f"Data transmitted successfully. ({len(frame.encode('utf-8'))})")
            self.it += 1

        except Exception as e:
            self.update_status(f"Error: {str(e)}", is_error=True)

    def read_data(self):
        while not self.stop_event.is_set():
            try:
                if self.serial_port and self.serial_port.is_open:
                    line = self.serial_port.readline().decode('utf-8').strip()
                    if line:
                        elements = line.split(';')
                        if len(elements) > 2:
                            data_list = elements[2:-1]
                            # Convert data to hexadecimal
                            hex_data_list = [hex(int(value)) for value in data_list]
                            data_string = ' '.join(hex_data_list)
                            self.root.after(0, self.update_received_data, elements[0], elements[1], data_string)
                        self.receive_text.insert(END, f"{self.it}. RX << {line}\n")
                        self.receive_text.see(END)
                        self.update_status(f"Data received successfully. ({len(line)})")
                        self.it += 1
            except Exception as e:
                self.update_status(f"Error: {str(e)}", is_error=True)

    def update_received_data(self, can_id, data_length, data):
        if can_id not in self.id_listbox.get(0, tk.END):
            self.id_listbox.insert(tk.END, can_id)
        # Store data for the selected ID
        if can_id not in self.data_store:
            self.data_store[can_id] = []
        self.data_store[can_id].append(f"Length: {data_length} Data: {data}")

    def on_id_select(self, event):
        try:
            selection = event.widget.curselection()
            if selection:
                index = selection[0]
                can_id = event.widget.get(index)
                self.data_display.delete(0, tk.END)
                for item in self.data_store.get(can_id, []):
                    self.data_display.insert(tk.END, item)
        except Exception as e:
            self.update_status(f"Error: {str(e)}", is_error=True)

    def update_status(self, message, is_error=False):
        if is_error:
            self.status_label.config(text=message, foreground="red")
        else:
            self.status_label.config(text=message, foreground="green")
        self.root.after(15000, self.clear_status)

    def clear_status(self):
        self.status_label.config(text="")

    def close(self):
        self.stop_event.set()
        try:
            if self.serial_port and self.serial_port.is_open:
                self.serial_port.close()
        except Exception as e:
            self.update_status(f"Error: {str(e)}")
        self.root.destroy()


if __name__ == "__main__":
    root = ttk.Window(themename="superhero")
    app = CANApp(root)
    root.protocol("WM_DELETE_WINDOW", app.close)
    root.mainloop()
