#include <assert.h>
#include "ring_buffer.h"


bool RingBuffer_Init(RingBuffer *ringBuffer, char *dataBuffer, size_t dataBufferSize) 
{
	assert(ringBuffer);
	assert(dataBuffer);
	assert(dataBufferSize > 0);
	
	if ((ringBuffer) && (dataBuffer) && (dataBufferSize > 0)) {
	    ringBuffer->buffer = dataBuffer;
	    ringBuffer->head = 0;
	    ringBuffer->tail = 0;
	    ringBuffer->size = dataBufferSize;
    	ringBuffer->element_count = 0;
    	return true;
	}
	
	return false;
}

bool RingBuffer_Clear(RingBuffer *ringBuffer)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		ringBuffer->head = 0;
		ringBuffer->tail = 0;
		ringBuffer->element_count = 0;
		return true;
	}
	return false;
}

bool RingBuffer_IsEmpty(const RingBuffer *ringBuffer)
{
    assert(ringBuffer);	
    if(ringBuffer->element_count > 0) 
        {
    	return false;
        }
	
	return true;
}

size_t RingBuffer_GetLen(const RingBuffer *ringBuffer)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		return ringBuffer->element_count;
	}
	return 0;
	
}

size_t RingBuffer_GetCapacity(const RingBuffer *ringBuffer)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		return ringBuffer->size;
	}
	return 0;	
}


bool RingBuffer_PutChar(RingBuffer *ringBuffer, char c)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
	    
	    if(ringBuffer->element_count == ringBuffer->size){
	        return false;
	    }
		
	    ringBuffer->element_count++;
	    ringBuffer->buffer[ringBuffer->head] = c;
	    
		ringBuffer->head+=1;
		
		if(ringBuffer->head == ringBuffer->size){
		    ringBuffer->head = 0;
		}
		
		return true;
	}
	return false;
}

bool RingBuffer_GetChar(RingBuffer *ringBuffer, char *c)
{
	assert(ringBuffer);
	assert(c);
	
  if ((ringBuffer) && (c)) {
		if(ringBuffer->element_count > 0)
		{
		    *c = ringBuffer->buffer[ringBuffer->tail];
		    ringBuffer->tail++;
		    
		    if(ringBuffer->tail == ringBuffer->size){
		        ringBuffer->tail = 0;
		    }
		
		    ringBuffer->element_count --;
		    return true;
		}
	}
	return false;
}
