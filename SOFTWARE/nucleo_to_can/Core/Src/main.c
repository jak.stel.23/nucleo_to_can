/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "fdcan.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#define  _WANT_IO_C99_FORMATS
#include <inttypes.h>
#include "ring_buffer.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#define LINE_MAX_LENGTH 200

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


/* Configure Tx buffer message */

//static FDCAN_FilterTypeDef sFilterConfig;
static FDCAN_TxHeaderTypeDef TxHeader;
static FDCAN_RxHeaderTypeDef RxHeader;
static uint8_t TxData[60];
static uint8_t RxData[60];

// UART receive buffer descriptor
static RingBuffer USART_RingBuffer_Rx;
// UART receive buffer memory pool
static char RingBufferData_Rx[2048];

static char UART_frame[LINE_MAX_LENGTH + 1];
static uint32_t line_length = 0;

uint8_t UART_RX_CPLT = 1;

typedef struct TIME{
	uint32_t LED1_prev_time;
	uint32_t LED2_prev_time;
	uint32_t LED3_prev_time;
	uint32_t on_time;
	uint32_t BT1_prev_time;
	uint32_t BT2_prev_time;
	uint32_t BT_debouncing_time;
}TIME;

static TIME time;
static uint8_t uart_rx_buffer;

__IO uint8_t UserButton1ClickEvent = RESET;
__IO uint8_t UserButton2ClickEvent = RESET;
/*
static volatile uint16_t CAN_STATE[2]; // H - L
static  uint8_t SWAP = 0;


void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc){
	if(hadc == &hadc4){
		if(CAN_STATE[0] < 2700 || CAN_STATE[1] > 2900)
			SWAP = 1;
		if(CAN_STATE[1] < 2700 || CAN_STATE[0] > 2900)
			SWAP = 0;
	}
}
*/

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  if (GPIO_Pin == B2_Pin)
  {
	  if( HAL_GetTick() - time.BT2_prev_time > time.BT_debouncing_time)
		  UserButton2ClickEvent = SET;
  }
  if (GPIO_Pin == B1_Pin)
  {
	  if( HAL_GetTick() - time.BT1_prev_time > time.BT_debouncing_time)
		  	 UserButton1ClickEvent = SET;
  }
}



void input_frame_processing()
{
	uint8_t value;
	if( RingBuffer_GetChar(&USART_RingBuffer_Rx, (char*)&value) == true){
		if (line_length < LINE_MAX_LENGTH) {
			if (value == '\r' || value == '\n') {
				if (line_length > 0) {
					UART_RX_CPLT = 0;
					UART_frame[line_length] = '\0';
					char* pos=(char*)UART_frame;
					char* prev_pos=(char*)UART_frame;
					char buf[LINE_MAX_LENGTH];

					line_length = 0;
					TxHeader.DataLength = 0;

					for(size_t i=0;;i++){
						if( (pos=strchr(pos,';')) != NULL){
							strncpy(buf,prev_pos,pos-prev_pos);
							buf[pos - prev_pos] = '\0';
							pos++;
							prev_pos = pos;
						}else
						  break;

						if(0 == i)
							sscanf(buf, "%" SCNu32, &TxHeader.Identifier);
						if(1 == i)
							sscanf(buf, "%" SCNu32, &TxHeader.DataLength);
						if(1 < i)
							sscanf(buf, "%" SCNu8,  &TxData[i - 2]);
						pos++;
					}

				}
			}
			else {
				if('\177' == value){ //backspace
					if(line_length>0)
						line_length--;
				}
				else
					UART_frame[line_length++] = value;
			}
			}else
			line_length = 0;

		}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  if (huart == &hlpuart1) {
	  __disable_irq();
	  RingBuffer_PutChar(&USART_RingBuffer_Rx, (char)uart_rx_buffer);
	  __enable_irq();
	  HAL_UART_Receive_IT(&hlpuart1, &uart_rx_buffer, 1);
	  time.LED3_prev_time = HAL_GetTick();
  }
}

uint8_t UART_TX_CPLT = 1;

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
	if(huart == &hlpuart1){
		UART_TX_CPLT=1;
		time.LED3_prev_time = HAL_GetTick();
	}
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_LPUART1_UART_Init();
  MX_ADC4_Init();
  MX_FDCAN1_Init();
  /* USER CODE BEGIN 2 */

  RingBuffer_Init(&USART_RingBuffer_Rx, RingBufferData_Rx, sizeof(RingBufferData_Rx));
  const char init_message[] = "-INIT-\n\r";
  //const char control[] = "CONT ";
  const char test[] = "-TEST-\n\r";
 // const char swapped[] = "-SWAPPED-\n\r";
  const uint8_t data_length_bytes[] = {0,1,2,3,4,5,6,7,8,12,16,20,24,32,48,64};

  char frame[LINE_MAX_LENGTH];


  while(HAL_UART_Transmit_IT(&hlpuart1, (uint8_t*)init_message, strlen(init_message))!= HAL_OK);
  //while(HAL_UART_Transmit_IT(&hlpuart1, (uint8_t*)prompt, strlen(prompt))!= HAL_OK);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  HAL_UART_Receive_IT(&hlpuart1, &uart_rx_buffer, 1);

  TxHeader.Identifier = 0x1;
  TxHeader.IdType = FDCAN_STANDARD_ID;
  TxHeader.TxFrameType = FDCAN_DATA_FRAME;
  TxHeader.DataLength = FDCAN_DLC_BYTES_12;
  TxHeader.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
  TxHeader.BitRateSwitch = FDCAN_BRS_ON;
  TxHeader.FDFormat = FDCAN_FD_CAN;
  TxHeader.TxEventFifoControl = FDCAN_STORE_TX_EVENTS;
  TxHeader.MessageMarker = 0x0;

 // while(HAL_FDCAN_ConfigFilter(&hfdcan1, &sFilterConfig) != HAL_OK);

  while(HAL_FDCAN_Start(&hfdcan1)!= HAL_OK);

  time.on_time = 100;
  time.BT_debouncing_time = 200;
  size_t len=0;

 //HAL_ADC_Start_DMA(&hadc4, (uint32_t*) CAN_STATE, 2);

  while (1)
  {
/*
	  //check if CAN_L or CAN_H is swapped
	  if(1 == SWAP && 1 == UART_TX_CPLT){
		  UART_TX_CPLT = 0;
  		  while(HAL_UART_Transmit_IT(&hlpuart1, (uint8_t*)swapped, strlen(swapped))!= HAL_OK);
	  }
	  //------------------------------------//
	  if(0 == SWAP){
	  */
		  if(SET == UserButton1ClickEvent && 1 == UART_TX_CPLT) {
		  		  UserButton1ClickEvent = RESET;
		  		  UART_TX_CPLT = 0;
		  		  UART_RX_CPLT = 0;
		  		  time.BT1_prev_time = HAL_GetTick();
		  		  while(HAL_UART_Transmit_IT(&hlpuart1, (uint8_t*)test, strlen(test))!= HAL_OK);
		  		  time.LED2_prev_time = HAL_GetTick();

		  		  TxHeader.Identifier=1;
		  		  TxHeader.DataLength=0;

		  	  	}


		  	if(0 == UART_RX_CPLT){
		  		while(HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan1, &TxHeader, TxData)!= HAL_OK);
		  		time.LED1_prev_time = HAL_GetTick();
		  		// if correct  transmit this frame to CAN

		  		UART_RX_CPLT = 1;
		  	}else
		  		input_frame_processing();

		  	//converting payload
		  	if(HAL_FDCAN_GetRxFifoFillLevel(&hfdcan1,FDCAN_RX_FIFO0) > 0 && UART_TX_CPLT == 1){
		  		UART_TX_CPLT = 0;
		  		while(HAL_FDCAN_GetRxMessage(&hfdcan1, FDCAN_RX_FIFO0, &RxHeader, RxData)!= HAL_OK);

		  		len = sprintf(frame,"%lu;%lu;",RxHeader.Identifier,RxHeader.DataLength);
		  		for(size_t i=0;i<data_length_bytes[RxHeader.DataLength];i++)
		  			len += sprintf(frame + len,"%d;",RxData[i]);
		  		sprintf(frame + len,"\n");

		  		while(HAL_UART_Transmit_IT(&hlpuart1, (uint8_t*)frame, strlen(frame))!= HAL_OK);
		  		time.LED2_prev_time = HAL_GetTick();
		  	}

		  	if(HAL_FDCAN_GetRxFifoFillLevel(&hfdcan1,FDCAN_RX_FIFO1) > 0 && UART_TX_CPLT == 1){
		  		UART_TX_CPLT = 0;
		  		while(HAL_FDCAN_GetRxMessage(&hfdcan1, FDCAN_RX_FIFO1, &RxHeader, RxData)!=HAL_OK);

		  		len = sprintf(frame,"%lu;%lu;",RxHeader.Identifier,RxHeader.DataLength);
		  		for(size_t i=0;i<data_length_bytes[RxHeader.DataLength];i++)
		  			len += sprintf(frame + len,"%d;",RxData[i]);
		  		sprintf(frame + len,"\n");

		  		while(HAL_UART_Transmit_IT(&hlpuart1, (uint8_t*)frame, strlen(frame))!= HAL_OK);
		  		time.LED2_prev_time = HAL_GetTick();
		  	}
	//  }


		  	if(HAL_GetTick() - time.LED1_prev_time > time.on_time)
		  		  HAL_GPIO_WritePin( LED1_GPIO_Port , LED1_Pin, GPIO_PIN_RESET);
		  	else
		  		  HAL_GPIO_WritePin( LED1_GPIO_Port , LED1_Pin, GPIO_PIN_SET);

		  	if(HAL_GetTick() - time.LED2_prev_time > time.on_time)
		  		  HAL_GPIO_WritePin( LED2_GPIO_Port , LED2_Pin, GPIO_PIN_RESET);
		  	else
		  		  HAL_GPIO_WritePin( LED2_GPIO_Port , LED2_Pin, GPIO_PIN_SET);

		  	if(HAL_GetTick() - time.LED3_prev_time > time.on_time)
				  HAL_GPIO_WritePin( LED3_GPIO_Port , LED3_Pin, GPIO_PIN_RESET);
			else
				  HAL_GPIO_WritePin( LED3_GPIO_Port , LED3_Pin, GPIO_PIN_SET);



    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV4;
  RCC_OscInitStruct.PLL.PLLN = 85;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
