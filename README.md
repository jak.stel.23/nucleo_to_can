# NUCLEO_TO_CAN

## Opis

Projekt umożliwiający wysyłanie, odbieranie oraz analizę danych magistrali CAN przy pomocy komputera, oraz łatwą implementację niskopoziomowych rozwiązań w przetwarzaniu danych przez mikrokontroler. Dzięki płytce NUCLEO oraz nakładce, użytkownik zyskuje szeroki wachlarz możliwości przetwarzania ramek CAN i komunikacji z komputerem. Projekt nakładki na NUCLEO jest kompatybilny z kilkoma różnymi wariantami platformy. Podstawowe oprogramowanie umożliwia odbiór i wysyłanie danych.

## 1. Co zawiera repozytorium? 

Poszczególne foldery odpowiadają za:

- **HARDWARE** - zawiera projekt płyty PCB NUCLEO_TO_CAN (schematy, PCB, zrzuty widoku 3D oraz zdjęcia zmontowanego układu).
- **SOFTWARE**:
    - **G474RE** - zawiera podstawowe oprogramowanie na platformę NUCLEO-64 G474RE, konwertujące informacje z UART na ramkę CAN.
    - **G491RE** - zawiera podstawowe oprogramowanie na platformę NUCLEO-64 G474RE, konwertujące informacje z UART na ramkę CAN (TODO).

## 2. Opis funkcjonalności

1. **BT1** - wysyłanie informacji <span style="color:lightblue">"-TEST-"</span> na port <span style="color:lightblue">UART</span> oraz wysyłanie informacji <span style="color:lightblue">"ID: 1, DATA: NULL [1;0;]"</span> na magistralę <span style="color:lightblue">CAN</span>.
2. **L1** - odbiór ramki z magistrali <span style="color:lightblue">CAN</span>.
3. **L2** - wysyłanie ramki na magistralę <span style="color:lightblue">CAN</span>.
4. **L3** - komunikacja <span style="color:lightblue">UART (RX/TX)</span>.
5. Format danych w ramach <span style="color:lightblue">UART</span> -> <span style="color:lightblue">"[ID];[LENGTH];[D1];[D2];...;[DN];"</span>, pusta ramka jest możliwa -> <span style="color:lightblue">"[ID];0;"</span>.

## 3. Scenariusze użycia

1. Użytkownik po wciśnięciu przycisku L1 na nakładce wysyła informacje na linię CAN oraz UART w celu sprawdzenia działania układu oraz magistrali.
2. Użytkownik konfiguruje ramkę z użyciem aplikacji napisanej w języku Python. Aplikacja zawiera także logger, który pozwala na zbieranie ramek, sortując je po [ID], oraz wyświetlacz danych odebranych na porcie COM.
3. Aplikacja może służyć jako monitor portu COM - w ramce są wyświetlane wszytskie informacje przesyłane portem szeregowym.

## 4. Wersja 1 Nakładki

**Projekt PCB:**

<img src="./HARDWARE/nucleo_to_can_top.png"  width="49%">
<img src="./HARDWARE/nucleo_to_can_bottom.png"  width="49%">

**Zmontowany układ + zamontowany na NUCLEO:**

<img src="./HARDWARE/nucleo_to_can_assembled.jpg"  width="98%">

Dzięki zmontowaniu kilku sztuk możliwe jest połączenie urządzeń i przesyłanie danych między wszystkimi - symulacja rzeczywistej topologii sieci wraz ze skonfigurowaną terminacją końcową.

## 5. Oprogramowanie

![Windows App](SOFTWARE/win_app.png)

### Opis aplikacji NUCLEO_TO_CAN V1.0

Aplikacja NUCLEO_TO_CAN V1.0 została stworzona w celu ułatwienia komunikacji pomiędzy płytką rozwojową NUCLEO a magistralą CAN za pośrednictwem portu UART (COM). Poniżej znajduje się opis jej głównych funkcji i interfejsu użytkownika.

#### Interfejs Użytkownika

1. **UART Selection**:
   - **UART Port**: Wybór portu UART, który będzie używany do komunikacji z płytką NUCLEO. Lista rozwijana umożliwia wybór dostępnych portów.
   - **Baud Rate**: Ustawienie prędkości transmisji danych przez UART.
   - **Open UART**: Przycisk do otwarcia połączenia UART.
   - **Close UART**: Przycisk do zamknięcia połączenia UART.
   - **Update Ports**: Przycisk do aktualizacji listy dostępnych portów UART.
   
2. **CAN Frame Settings**:
   - **Identifier**: Pole do wprowadzenia identyfikatora ramki CAN.
   - **Data Length**: Ustawienie długości danych ramki CAN (opcje dostępne w liście rozwijanej).
   - **Word Length**: Ustawienie długości słowa danych (opcje dostępne w liście rozwijanej).
   - **Data**: Pole do wprowadzenia danych, które mają być wysłane w ramce CAN.
   - **Send**: Przycisk do wysłania skonfigurowanej ramki CAN.

3. **Komunikacja UART**:
   - Pod przyciskami związanymi z ustawieniami UART znajduje się okno, w którym wyświetlana jest bieżąca komunikacja odbywająca się przez UART. Jest to miejsce, gdzie można obserwować wysyłane i odbierane dane w czasie rzeczywistym.

4. **Received Data**:
   - **Received IDs**: W tej sekcji wyświetlane są identyfikatory odebranych ramek CAN.
   - **Data Display**: Sekcja wyświetlająca dane odebrane w ramach CAN, przypisane do odpowiednich identyfikatorów.

#### Procedura Użytkowania

1. **Połączenie za pomocą UART**:
   - Na początku należy wybrać odpowiedni port UART oraz ustawić prędkość transmisji danych (Baud Rate).
   - Następnie, za pomocą przycisku "Open UART", otwieramy połączenie z płytką NUCLEO.
   
2. **Konfiguracja i Wysyłanie Ramki CAN**:
   - W sekcji "CAN Frame Settings" wprowadzamy identyfikator ramki, długość danych oraz długość słowa danych.
   - W polu "Data" wpisujemy dane, które chcemy wysłać. Dane mogą być wprowadzone w systemie ósemkowym, szesnastkowym lub dziesiętnym.
   - Po skonfigurowaniu wszystkich parametrów klikamy przycisk "Send" w celu wysłania ramki CAN.

3. **Odbiór Danych**:
   - Otrzymane ramki CAN są wyświetlane w sekcji "Received Data". Identyfikatory ramek są pokazywane w kolumnie "Received IDs", a odpowiednie dane w kolumnie "Data Display".

Aplikacja umożliwia także walidację wprowadzonych danych pod kątem ich spójności, co zapewnia poprawność przesyłanych informacji.

#### Dodatkowe Informacje

Aby zapewnić poprawne działanie aplikacji, należy upewnić się, że połączenia są stabilne oraz że wybrane porty i prędkości transmisji są zgodne z ustawieniami płytki NUCLEO.

W aktualnej wersji aplikacji, zmiana parametrów peryferium, takich jak długość słowa czy prędkość, jest niemożliwa i odbywa się za pomocą CubeMX. Wybór długości słowa w aplikacji służy jedynie do walidacji danych. W przyszłości konfiguracja będzie możliwa z poziomu aplikacji, a dodatkowo zostaną dodane ramki sterujące.

## N. TODO

W wersji 2 projektu:

- Dodać diody na liniach CANRX oraz CANTX.
- Dodać filtr LP na linię CANTX (opcjonalne).
- Zamienić złącza ARK 2.54mm na ARK 5mm - łatwiejsze w obsłudze.
- Oddalić od siebie zworki konfigurujące terminację.

## Podsumowanie

Projekt został stworzony jako prosty logger dla transmisji danych po magistrali CAN. Dzięki zastosowaniu płytki ewaluacyjnej NUCLEO pozwala on na zarządzanie magistralą na bardzo niskim poziomie. Mikrokontroler na płytce NUCLEO posiada wbudowane peryferia CAN, a zworki umożliwiają testowanie różnych konfiguracji transceivera. Dzięki wykorzystaniu UART, urządzenie jest funkcjonalne oraz bardzo proste w oprogramowaniu i użytkowaniu.

Projekt jest przyszłościowy i ze względu na swoją przydatność oprogramowanie będzie w przyszłości aktualizowane oraz będą dodawane nowe funkcje. Kolejnym plusem jest fakt, że przy tworzeniu projektu z użyciem magistrali CAN na tej platformie można napisać testowy kod bez konieczności tworzenia prototypu.


Link do repozytorium:
https://gitlab.com/jak.stel.23/nucleo_to_can